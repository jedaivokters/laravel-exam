# Set up

1. Inside project directory run ```composer install```
2. (optional) Rename ```local.env``` to ```.env``` *Note: Check file for DB config*
3. (optional) Run ```php artisan key:generate```
4. Run ```php artisan migrate```
5. Run ```php artisan serve``` ***Note: Make sure default port is open***
6. Browse: http://127.0.0.1:8000